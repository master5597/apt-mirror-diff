# Apt Mirror Diff

The ultimate goal of this project to to create bite sized update packages for specified Debian repositories without needing to keep a whole copy of the remote repository locally.  (Intended for off-line updates & python 3 based)

Currently this project is kept to a single monolithic file and doesn't use any debian apt libraries so that it can be easily copied and run on just about any system with python 3.x.  This python project is also a training/learning exercise for the initial developer.  This project intends to implement PEP484 and (a) documentation standard(s) (sphinx?).

This project takes some of it queues from the widely used apt-mirror perl script (https://github.com/apt-mirror/apt-mirror) and may support some or all of it's options in the future.