#!/usr/bin/env python3
import _hashlib  # for checksums
import bz2  # for Package decompression
import gzip  # for Package decompression
import json  # for output list
import lzma  # for Package decompression
import sys  # to check python version
from configparser import ConfigParser  # for meta and config files
from datetime import date  # for current date
from datetime import datetime as dt  # for timestamp for zip file
from hashlib import md5, sha1, sha256, sha384, sha512  # for checksums
from hashlib import new as hashlib_new, algorithms_available  # for checksums
from os.path import basename, dirname, join, normpath  # path stuff
from pathlib import Path  # more path stuff
from platform import machine  # to get default arch TODO: check if names match?
from queue import Empty, PriorityQueue, Queue  # for multi-threading downloads
from threading import Thread  # for multi-threading downloads
from time import sleep  # for downloading wait.
from typing import Optional, List, Dict, Set  # for PEP 484 & documentation
from urllib.parse import urljoin, urlparse  # to build urls
from urllib.request import urlopen  # to download
from zipfile import ZIP_DEFLATED, ZipFile  # for output zip file(s)


# Python 3.7 is required to keep option dictionary ordered properly
# if sys.version_info<(3,7,0):
#    print("This script requires python 3.7 or above.  You are running python"
#          " %s.%s" % (sys.version_info[0], sys.version_info[1]))
#    sys.exit(-1)

# TODO: PEP484
# TODO: reStructuredText/Sphinx
# TODO: Command-line options and/or configuration file
# TODO: Support rfc822 style format for sources.list


class Source:
    """
    # Arch lists taken from https://wiki.debian.org/SupportedArchitectures
    all_supported_archs = ["Armel", "armhf", "arm64", "i386", "amd64",
                           "mips", "mipsel", "PowerPC", "s390x",
                           "kfreebsd-i386", "kfreebsd-amd64"]
    all_known_archs = all_supported_archs + \
        ["Alpha", "Arm", "hppa", "ia64", "s390", "sh", "sparc",
            "hurd-i386", "netbsd-i386", "netbsd-alpha"]
    """

    def __init__(self, src_type, options, uri, distribution, components=[]):
        """ creates a source object based on the given parameters
            if no 'arch' option passed, machine() assumed.
        """
        self.type = src_type
        # TODO: convert all option keys to lowercase
        self.options = options if options else {}
        self.uri = uri
        self.distribution = distribution
        self.components = components
        # Set the default architecture
        if "arch" not in self.options.keys():
            if machine() == "x86_64":
                self.options["arch"] = ["amd64"]
            else:  # TODO: find out if any others need manually set.
                self.options["arch"] = [machine()]

    def get_norm_option_list(self, option_type, option_all, update=True):
        """ This function goes through and option list and calculates & returns
            the effective option(s) based on the passed '+', '-', & 'all'
            options
        """
        if option_type not in self.options.keys():
            return list()
        retval = set()
        for option_value in self.options[option_type]:
            # print("processing option: %s" % option_value)
            if option_value == "-all":
                for option in option_all:
                    retval.discard(option)
            elif option_value == "+all":
                for option in option_all:
                    retval.add(option)
            elif option_value[0] == "-":
                retval.discard(option_value[1:])
            elif option_value[0] == "+":
                retval.add(option_value[1:])
            elif option_value == "all":
                retval.update(option_all)
            else:  # just a normal value
                retval.add(option_value)
        if update:
            self.options[option_type] = list(retval)
        # print("get_normailized_option_list returning: %s", list(retval))
        return list(retval)

    # TODO: remove function?  or update?
    def old_get_all_release_urls(self):
        """ returns a tuple containing the urls to the default
        (InRelease, Release Release.gpg) files.
        """
        meta_tuple = ()
        for append in ["InRelease", "Release", "Release.gpg"]:
            meta_tuple += (
                urljoin(
                    self.uri, "/".join(["dists", self.distribution, append])
                ),
            )
        return meta_tuple

    # TODO: if not removing function above use it instead?
    def get_all_base_metadata_repofiles(self) -> list:
        file_list: List[RepoFile] = list()
        for meta_name in ["InRelease", "Release", "Release.gpg"]:
            print(
                "adding to file_list:",
                self.uri if self.uri.endswith("/") else self.uri + "/",
                "/".join(["dists", self.distribution, meta_name]),
            )
            # TODO: Fix this mess.
            # NOTE: If urljoin base doesn't end with a '/' the parts after
            #       the last '/' is deleted
            file_list.append(
                RepoFile.from_url(
                    urljoin(
                        self.uri if self.uri.endswith("/") else self.uri + "/",
                        "/".join(["dists", self.distribution, meta_name]),
                    ),
                    self,
                )
            )
        print("get_all...repofiles.file_list:", file_list)
        return file_list

    def string_to_bool(self, text: str, default: bool) -> bool:
        """ string_to_bool - Converts a string into a boolean
        This inspects the string to see if it is true or if it is false and
        then returns the result. Several variants on true / false are checked.

        :param text: The text to be checked for boolean value
        :type text: str
        :param default: If a determination cannot be made, return this
        :type default: bool
        :returns: a bolean representation of the text (or default)
        :rtype: bool
        """
        # only check strings (python 3)
        if not isinstance(text, str):
            return default
        # Check for positives
        if (
            text.lower() == "no"
            or text.lower() == "false"
            or text.lower() == "without"
            or text.lower() == "off"
            or text.lower() == "disable"
        ):
            return True
        # Check for negatives
        if (
            text.lower() == "yes"
            or text.lower() == "true"
            or text.lower() == "with"
            or text.lower() == "on"
            or text.lower() == "enable"
        ):
            return False
        # didn't match anything go with default
        return default

    def get_boolean_option(self, option_type, default_value):
        if option_type not in self.options.keys():
            return default_value
        return self.string_to_bool(self.options[option_type], default_value)

    def options_as_str(self):
        """ for debug purposes returns options as a string
            key order not guaranteed unless using python 3.7+ """
        if not self.options:
            return ""
        retval = "["
        for key in self.options:
            for value in self.options[key]:
                if value[0] == "-":
                    retval += key + "-=" + value[1:]
                elif value[0] == "+":
                    retval += key + "+=" + value[1:]
                else:
                    retval += key + "=" + value
                retval += " "
        retval = retval.strip()
        if len(retval) > 1:
            return retval + "]"
        return ""

    def is_binary(self):
        return not self.type.endswith("-src")

    def __str__(self) -> str:
        """ For debug purposes returns options as a string
            should be a valid line with options expanded """
        return (
            self.type
            + " "
            + self.options_as_str()
            + " "
            + self.uri
            + " "
            + self.distribution
            + (
                " " + " ".join(self.components)
                if len(self.components) > 0
                else ""
            )
        )


# TODO: transition checksums to this Checksums class
class Checksums:
    """
    This Checksums class is an attempt at simplifying all the checksumming
    needs of this script
    """

    default_algorithms: Set[str] = set(
        ["md5", "sha1", "sha256", "sha384", "sha512"]
    )

    # TODO: how to PEP484 for <class '_hashlib.HASH'>
    @staticmethod
    def calculate_hash(raw_data: bytes, hash_func: _hashlib.HASH) -> str:
        """
        Returns the hexdigest for the passed hash_func
        and raw data -> hash_func.hexdigest()

        :param raw_data: The bytes to be hashed
        :param hash_func: The name of the function to be utilized. (eg. md5)
                          see hashlib.algorithms_available
        :return: the hashed value as a string (see hash_func.hexdigest()
        """
        for chunk in [
            raw_data[i : (i + 4096)] for i in range(0, len(raw_data), 4096)
        ]:
            hash_func.update(chunk)

        return hash_func.hexdigest()

    @staticmethod
    def calculate_list(
        raw_data: bytes, algorithm_names: Set[str]
    ) -> Dict[str, str]:
        """
        Returns a Dict of hash_name->value and 'size'->value for the passed
        raw_data and the algorithms passed

        :param raw_data: The bytes to be hashed
        :param algorithm_names: The set of algorithms used to hash the raw_data
        :return: Dict[str,str] with keys being the hash_name or 'size'
        """
        retval: Dict[str, str] = dict()
        for hash_name in algorithm_names:
            retval[hash_name] = Checksums.calculate_hash(
                raw_data, hashlib_new(hash_name)
            )
        retval["size"] = str(len(raw_data))
        return retval

    @staticmethod
    def calculate_defaults(raw_data: bytes) -> Dict[str, str]:
        """
        Returns a Dict of hash_name->value and 'size'->value for the hashes
        listed in self.default_algorithms

        :param raw_data: The bytes to be hashed
        :return: Dict[str,str] with keys being the hash_name or 'size'
        """
        return Checksums.calculate_list(raw_data, Checksums.default_algorithms)

    @staticmethod
    def calculate_all(raw_data: bytes) -> Dict[str, str]:
        """
        Returns a Dict of hash_name->value and 'size'->value for the hashes
        listed in hashlib.algorithms_available
        :param raw_data: The bytes to be hashed
        :return: Dict[str,str] with keys being the hash_name or 'size'
        """
        return Checksums.calculate_list(raw_data, set(algorithms_available))


class RepoFile:
    def __init__(self, src, opts=dict()):
        self.src = src
        self.opts = opts.copy()
        self.raw_data: bytes = b""

    @classmethod
    def from_url(cls, url: str, src, options: Dict[str, str] = dict()):
        # don't modify the passed option.  it will modify the default.
        opts = options.copy()
        # print("call -> RepoFile.from_url(", url, ",", src, ",", options, ")")
        # import traceback
        # traceback.print_stack(file=sys.stdout)
        parsed_url = urlparse(url)
        # print("parsed_url:", parsed_url)
        if "name" not in opts.keys():
            opts["name"] = basename(parsed_url.path)
        dir_name = dirname(parsed_url.path).strip("/")
        # TODO: find better way to parse/unparse URL
        if "remote_path" not in options.keys():
            opts["remote_path"] = (
                parsed_url.scheme
                + "://"
                + parsed_url.netloc
                + "/"
                + dir_name
                + "/"
            )
        if ":" in parsed_url.netloc:
            # Remove the port number, more pythonic way?
            dns_name = parsed_url.netloc.split(":")[0]
        else:
            dns_name = parsed_url.netloc
        # print("dns_name=", dns_name, "dir_name=", dir_name)
        # print("join:", join(dest_mirror_dir, dns_name, dir_name, ''))
        if "zip_path" not in opts.keys():
            opts["zip_path"] = normpath(join(dns_name, dir_name, ""))
        # print("name=", name, "remote_path=",
        #      remote_path, "local_path=", local_path)
        # print("x src:", src)
        # print("opts:", opts)
        return cls(src, opts=opts)

    def download(self, retries=None, overwrite=None, verify=None) -> bool:

        # TODO: there has to be a better way
        if not retries:
            retries = self.opts.get("retries", 3)  # TODO set back to 3
        if not overwrite:
            overwrite = self.opts.get("overwrite", False)  # TODO: use option?
        if not verify:
            verify = self.opts.get("verify", False)

        remaining_retries = retries
        while remaining_retries > 0:
            try:
                # print("Downloading:", self.get_remote_path())
                my_req = urlopen(self.get_remote_path())
                self.raw_data = my_req.read()
                # TODO: is utf-8 correct?  other options below.
                # self.raw_data = my_req.read().decode("utf-8")
                # self.raw_data = "".join(map(chr, my_req.read()))
                # self.raw_data = str(my_req.read())
                # print("Successfully downloaded: " +
                #      self.get_remote_path())
                sleep(0.1)
            # TODO: Any others I shouldn't catch?
            except (KeyboardInterrupt, SystemExit):
                raise
            except Exception as e:
                if not self.get_remote_path().endswith("Packages"):
                    print(
                        "Error downloading "
                        + self.get_remote_path()
                        + " on trial no: "
                        + str(retries + 1 - remaining_retries)
                        + " Error:",
                        e,
                    )
                remaining_retries = remaining_retries - 1
                self.raw_data = None
                continue
            else:
                break
        # TODO: Clean this up, so messy
        if verify:
            return (
                self.raw_data is not None
                and self.calculate_update_hash("md5", md5())
                and self.calculate_update_hash("sha1", sha1())
                and self.calculate_update_hash("sha256", sha256())
                and self.calculate_update_hash("sha384", sha384())
                and self.calculate_update_hash("sha512", sha512())
                and self.calculte_update_size()
            )
        return self.raw_data is not None

    def calculate_update_hash(self, option, hash_type):
        with open(self.raw_data, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_type.update(chunk)
        if self.opts[option] is None:
            self.opts[option] = hash_type.hexdigest()
        elif self.opts[option] != hash_type.hexdigest():
            return False
        return True

    def calculte_update_size(self):
        if self.opts["size"] is None:
            self.opts["size"] = len(self.raw_data)
        elif self.opts["size"] != len(self.raw_data):
            return False
        return True

    def get_zip_path(self) -> str:
        if self.opts["zip_path"] and self.opts["name"]:
            return normpath(join(self.opts["zip_path"], self.opts["name"]))
        return ""

    def get_remote_path(self) -> str:
        if self.opts["remote_path"] and self.opts["name"]:
            return urljoin(self.opts["remote_path"], self.opts["name"])
        return ""

    def is_metafile(self) -> bool:
        if (
            self.opts["name"] in ("InRelease", "Release", "Release.gpg")
            or self.opts["name"] == "Packages"
            or self.opts["name"].startswith("Packages.")
        ):
            return True
        return False

    def get_hashes(self) -> Dict[str, str]:
        """returns the related hashes of a repofile.

    :return: Dictionary of hash types and values or empty dict.
        :rtype: Dict[str str]:

        """
        return self.opts.get("hashes", dict()).copy()

    def __lt__(self, _) -> bool:
        """This sets metafiles to a higher priority in the download queue.

        :param _: Unused Parameter.
        :type _: Any
    :return: Identical to is_metafile() (calls is_metafile()).
        :rtype: bool

        """
        return self.is_metafile()

    def __str__(self) -> str:
        """ for debug purposes returns options as a string """
        import pprint

        return pprint.pformat(self.opts, indent=4)


class ListParser:
    def parse_value(self, value: str, modifier: str = "") -> List[str]:
        """ splits value on ',' if exists. returns list of elements prepended
            with modifier """
        retval: List[str] = []
        if "," in value:
            tmp = value.split(",")
            for val in tmp:
                retval.append(modifier + val)
        else:
            retval.append(modifier + value)
        return retval

    def parse_options(self, line: str) -> Dict[str, List[str]]:
        """ A parser for the options in sources.list
            Options in sources.list are surrounded by '[]' """
        # print("parse_options(%s)" % line)
        options: Dict[str, List[str]] = {}
        parts = line.strip().split()
        for part in parts:
            if "-=" in part:
                (key, val) = part.split("-=")
                value = self.parse_value(val, "-")
            elif "+=" in part:
                (key, val) = part.split("+=")
                value = self.parse_value(val, "+")
            elif "=" in part:
                (key, val) = part.split("=")
                value = self.parse_value(val)
            else:
                print("Bad Option: '%s' in '%s' dropped" % (part, line))
                (key, value) = ("", list())
            if key and value and len(key) > 0 and len(value) > 0:
                # if it was a +/- and key exists add it, otherwise create it.
                if key in options and (
                    value[0][0] == "+" or value[0][0] == "-"
                ):
                    options[key] += value
                else:
                    options[key] = value
        return options

    def parse_line(self, line: str) -> Optional[Source]:
        """Sparse a given sources.list (textual) line and break it up
            into the fields we have

        :param line: Description of parameter `line`.
        :type line: str
    :return: A source object corresponding to the passed line
        :rtype: Source

        """
        line = line.strip()
        # print(line)
        # check if the source is empty or disabled
        if line == "" or line[0] == "#":
            # print("skipping empty or disabled line: %s" % line)
            return None
        # remove anything after a "#" (aka a comment)
        i = line.find("#")
        if i > 0:
            # Note: comment is line[i + 1:]
            line = line[:i]
        # if we have options split them out
        # per the docs type and options are spearated by a single space
        opt_open = line.find("[")
        opt_close = line.find("]")
        first_space = line.find(" ")
        if first_space + 1 != opt_open and opt_open != -1 and opt_close != -1:
            print("Invalid line: %s" % line)
            return None
        options = None
        if opt_open != -1 and opt_close != -1:
            options = self.parse_options(line[(opt_open + 1) : opt_close])
            line = line[:opt_open] + line[(opt_close + 1) :]
        elif opt_open == -1 and opt_close == -1:
            # print("No options to parse")
            pass
        elif opt_open == -1 or opt_close == -1:
            print("Invalid options on line: %s" % line)
            return None
        # source is ok so far, split it and see what we have
        pieces = line.split()
        # Sanity check
        if len(pieces) < 3:
            print("Invalid parameters on line: %s" % line)
            return None
        # Type, deb or deb-src.  (spec supports rpm & rpm-src)
        # TODO: add type rpm & rpm-src support
        repo_type = pieces[0].strip()
        # Sanity check
        if repo_type not in ("deb", "deb-src"):
            # TODO: support apt-mirror syntax of 'deb-arch'?
            print("Invalid or currently unsupported repo type: %s" % line)
            return None
        # URI
        uri = pieces[1].strip()
        if len(uri) < 1:
            print("Invalid uri: %s" % line)
            return None
        # distro (or directory)
        distro = pieces[2].strip()
        if len(distro) < 1:
            print("Invalid distro: %s" % line)
            return None
        # Components (optional)
        if len(pieces) > 3:
            # List of components
            components = pieces[3:]
        else:
            components = []
        return Source(
            src_type=repo_type,
            options=options,
            uri=uri,
            distribution=distro,
            components=components,
        )

    def parse_file(self, file: Path) -> List[Source]:
        """Simple converts the passed Path into an array of
        Source objects one per line.
        Lines are parsed via the parse_line() function in this class.

        :param file: Path to be opened and read from.
        :type file: Path
        :return: A list Source objects one per line in the passed 'file'.
        :rtype: list(Source)

        """
        sources: List[Source] = list()
        with open(str(file.resolve()), "r", encoding="utf-8") as fileptr:
            for line in fileptr:
                source = self.parse_line(line)
                if source:
                    sources.append(source)
        return sources


class MetaData:
    def __init__(self, rpofile: RepoFile):
        # print("MetaData(", rpofile, ")")
        self.type: str = ""
        self.arch_list: List[str] = list()
        self.meta_info: Dict[str, str] = dict()
        self.component_list: List[str] = list()
        self.meta_info["files"] = list()
        self.rpofile: RepoFile = rpofile
        if not rpofile.raw_data:
            print("Bad things, no file data for:", rpofile.opts["name"])
        self.parse_rpofile()

    def parse_rpofile(self) -> None:
        if not self.rpofile.raw_data:
            return
        # TODO: is Release.pgp really a release file?
        if self.rpofile.opts["name"] in (
            "InRelease",
            "Release",
            "Release.gpg",
        ):
            self.type = "release"
            self.parse_release()
        elif self.rpofile.opts["name"] == "Packages" or self.rpofile.opts[
            "name"
        ].startswith("Packages."):
            self.type = "package"
            self.parse_package()

    def parse_package(self) -> None:
        print("enter parse_package()")
        self.arch_list = self.rpofile.src.options["arch"]
        if self.rpofile.opts["name"] == "Packages":
            pkg_txt = self.rpofile.raw_data.decode("utf-8")
        else:
            # TODO: create decompress function that works based on filename
            # print("Decompressing file:", self.rpofile.opts["name"])
            if self.rpofile.opts["name"] == "Packages.gz":
                pkg_txt = gzip.decompress(self.rpofile.raw_data).decode(
                    "utf-8"
                )
            elif self.rpofile.opts["name"] == "Packages.bz2":
                pkg_txt = bz2.decompress(self.rpofile.raw_data).decode("utf-8")
            elif self.rpofile.opts["name"] == "Packages.xz":
                pkg_txt = lzma.decompress(self.rpofile.raw_data).decode(
                    "utf-8"
                )
            else:
                print("Bad package type: ", self.rpofile.opts["name"])
                pkg_txt = ""  # TODO: now what?

        tmp_info = dict()
        tmp_hashes = dict()
        for line in str(pkg_txt).splitlines():
            # print("checking line:", "'" + line + "'")
            if line.startswith("Filename: "):
                # print("** filename **")
                tmp_info["name"] = line[10:]
            elif line.startswith("Size: "):
                # print("** size **")
                tmp_info["size"] = line[6:]
            elif line.startswith("MD5sum: "):
                # print("** md5 **")
                tmp_hashes["md5sum"] = line[6:]
            elif line.startswith("SHA"):
                # print("** sha **")
                tmp_hashes[line[: line.find(":")].lower()] = line[
                    (line.find(": ") + 2) :
                ]
            elif not line.strip():  # space between sections
                if len(tmp_info) < 2 or len(tmp_hashes) == 0:
                    print("**Bad section**")
                    print(tmp_info)
                    print(tmp_hashes)
                    # exit(1)
                    # continue
                else:
                    """
                    tmplist = list(
                        tmp_info["name"], tmp_info["size"], tmp_info["hashes"])
                    self.meta_info["files"].append(tmplist)
                    """
                    self.meta_info["files"].append(
                        [tmp_info["name"], tmp_info["size"], tmp_hashes]
                    )
                # reset to clean for next loop
                tmp_info.clear()
                tmp_hashes.clear()

    def parse_release(self) -> None:
        print("enter parse_release()")
        config = ConfigParser()
        file_txt = self.rpofile.raw_data
        # TODO: figure out a way to verify the signature without requiring
        #       an additional (non-default) library, right now, just remove
        #       it or ignore the 2nd file in the list
        begin_txt = "-----BEGIN PGP SIGNED MESSAGE-----".encode("utf-8")
        begin_txt2 = "-----BEGIN PGP SIGNATURE-----".encode("utf-8")
        end_txt = "-----BEGIN PGP SIGNATURE-----".encode("utf-8")
        if begin_txt in file_txt:
            file_txt = file_txt[
                file_txt.find(begin_txt)
                + len(begin_txt) : file_txt.find(end_txt)
            ]
        if begin_txt2 in file_txt:
            file_txt = file_txt[
                file_txt.find(begin_txt2)
                + len(begin_txt2) : file_txt.find(end_txt)
            ]

        # nothing to process, quit processing
        if not file_txt or file_txt.isspace():
            print("exit parse_release() on file_text issue")
            return

        # add fake section header as ConfigParser requires a section header
        config_string = "[default]\n".encode("utf-8") + file_txt

        # print("STRING:", config_string)
        config.read_string(config_string.decode("utf-8"))
        # TODO: figure out type for meta_info and stick to it.  don't add and remove stuff.
        self.meta_info = dict(config.items("default"))

        checksums = dict()
        old_keys = list()
        for key in self.meta_info.keys():
            if key.startswith("sha") or key == "md5sum":
                self.fixup_checksum(key)
                checksums[key] = self.meta_info[key]
                # print("self.meta_info[key]:", self.meta_info[key])
                # print("checksums:", checksums)
                old_keys.append(key)

        [self.meta_info.pop(k, None) for k in old_keys]
        # del self.meta_info[old_keys]
        self.meta_info["files"] = self.colease_hash_lists(checksums)
        # print("self.meta_info:", self.meta_info)
        # exit(1)
        for list_item in ["architectures", "components"]:
            self.make_list(list_item)
        self.arch_list = self.rpofile.src.get_norm_option_list(
            "arch",
            (
                self.meta_info["architectures"]
                if "architectures" in self.meta_info
                else list(self.meta_info["architecture"])
            ),
        )
        avail_comps = (
            self.meta_info["components"]
            if "components" in self.meta_info
            else [self.meta_info["component"]]
        )
        print("avail_comps:", avail_comps)
        if not set(self.rpofile.src.components).issubset(avail_comps):
            diff_values = list(
                set(avail_comps).difference(self.rpofile.src.components)
            )
            print(
                "WARNING: Some requested components are unavailable:",
                diff_values,
            )
        acl_set = set(avail_comps)
        sc_set = set(self.rpofile.src.components)
        # check length
        if len(acl_set.intersection(sc_set)) > 0:
            self.component_list = acl_set.intersection(sc_set)
            print("self.component_list:", self.component_list)
        else:
            print("using empty component_list aka ERROR!")
            self.component_list = list()
        print("normal exit parse_release()")

    def make_list(self, key_name: str) -> None:
        if key_name in self.meta_info:
            self.meta_info[key_name] = list(self.meta_info[key_name].split())

    def fixup_checksum(self, key_name: str) -> None:
        if key_name in self.meta_info:
            cleaned_checksums: List[str] = list()
            for line in self.meta_info[key_name].splitlines():
                if len(line.split()) > 0:
                    cleaned_checksums.append(line.split())
            self.meta_info[key_name] = cleaned_checksums

    def colease_hash_lists(self, lists: dict = dict()) -> list:
        # orig hashes format: hash, size, path
        # list of Lists. inner list -> url, size, dict(hashes)
        retval = list()
        for key in lists.keys():
            for row in lists[key]:
                self.add_update_list(retval, key, row)
        return retval

    def add_update_list(self, data, hashkey, hashrow) -> None:
        found = False
        for row in data:
            if row[0] == hashrow[2]:
                row[2][hashkey] = hashrow[0]
                found = True
        if not found:
            tdct = dict()  # TODO: clean this up
            tdct[hashkey] = hashrow[0]
            data.append([hashrow[2], hashrow[1], tdct])

    def get_file_list(self) -> List[RepoFile]:
        print("Enter get_file_list()")
        print("self.component_list", self.component_list)
        if not self.rpofile.raw_data:
            return list()
        file_list = self.meta_info["files"]
        # if self.type == "package":
        #    print("#1 file_list:", file_list)
        # print("#1 position")
        file_list = self.filter_for_components(file_list)
        # if self.type == "package":
        #    print("#2 file_list:", file_list)
        file_list = self.filter_out_translations(file_list)
        # print("#3 file_list:", file_list)
        if self.rpofile.src.is_binary():
            # print("is_binary")
            file_list = self.filter_non_arch(file_list)
            # print("#3a file_list:", file_list)
            file_list = self.filter_src(file_list, False)
        else:
            # print("not is_binary")
            file_list = self.filter_src(file_list, True)
        # print("#4 file_list:", file_list)
        retval: List[RepoFile] = list()
        for row in file_list:
            options = dict()
            options["size"] = row[1]
            options["hashes"] = row[2]
            if self.type == "package":
                # TODO: this check for trailing '/' needs moved.
                #       maybe rpofile.src.url?
                retval.append(
                    RepoFile.from_url(
                        urljoin(
                            self.rpofile.src.uri
                            if self.rpofile.src.uri.endswith("/")
                            else self.rpofile.src.uri + "/",
                            row[0],
                        ),
                        self.rpofile.src,
                        options=options,
                    )
                )
            else:
                retval.append(
                    RepoFile.from_url(
                        urljoin(self.rpofile.opts["remote_path"], row[0]),
                        self.rpofile.src,
                        options=options,
                    )
                )
        return retval

    def filter_for_components(self, checksum_list) -> list:
        retval = list()
        print("Enter filter_for_components")
        # print("component filter vars:", vars())
        if not self.component_list:
            # print("Empty component list returning empty list!")
            # return retval
            # if not components accept all?
            return checksum_list
        print("component list:", self.component_list)
        components = tuple(
            [component + "/" for component in self.component_list]
        )
        for row in checksum_list:
            if row[0].startswith(components):
                retval.append(row)
        return retval

    def filter_non_arch(self, checksum_list: list) -> list:
        arch_filters = ["binary-", "Components-", "Contents-"]
        filter_list = list()
        for arch_filter in arch_filters:
            filter_list += [
                "/" + arch_filter + arch for arch in self.arch_list
            ]
        # print("filter_list:", filter_list)
        retval = list()
        for row in checksum_list:
            if any(s in row[0] for s in filter_list):
                # print("add1:", row)
                retval.append(row)
            elif not any(s in row[0] for s in arch_filters):
                # print("add2:", row)
                retval.append(row)
            # else:
            # print("rejecting:", row)
        return retval

    def filter_out_translations(
        self, checksum_list, exceptions: List[str] = ["en"]
    ) -> list:
        retval = list()
        # TODO: filter out language-pack-
        trans_prefix = "i18n/Translation-"
        filter_list = [trans_prefix + exception for exception in exceptions]
        for row in checksum_list:
            if any(s in row[0] for s in filter_list):
                retval.append(row)
            elif trans_prefix not in row[0]:
                retval.append(row)
        return retval

    def filter_src(self, checksum_list, keep_src) -> list:
        retval = list()
        for row in checksum_list:
            src_row = basename(dirname(row[0])).lower() == "source"
            if keep_src and src_row:
                retval.append(row)
            elif not keep_src and not src_row:
                retval.append(row)
        return retval

    def __str__(self) -> str:
        """ for debug purposes returns options as a string """
        # return (str(self.release_files))
        import pprint

        return pprint.pformat(self.release_files, indent=4)


class DownloadFiles:
    """
        This class manages all the downloads, it has multiple threads for
        downloading and a single thread to write the files into zips of a
        certain maximum size.
    """

    # TODO: is there a better way?
    def __init__(self) -> None:
        self.date_time: str = dt.now().strftime("%Y-%m-%d_%H:%M")
        self.__dl_threads: List[Thread] = list()
        self.__num_dl_threads: int = 10  # TODO: change back to 10
        # self.__zip_thread: Thread = None  # TODO: None isn't allowed
        # self.__zip_thread: Thread
        self.__skip_url_set: Set[str] = set()
        self.__skip_regex_set: Set[str] = set()
        self.__skip_meta_dict: Dict[str, Dict[str, str]] = dict()
        self.__download_url_set: Set[str] = set()
        self.__queued: PriorityQueue[RepoFile] = PriorityQueue()
        self.__downloaded: Queue[RepoFile] = Queue()
        self.__dest_zip_dir: str = ""
        self.__unclosed_zips: List[ZipFile] = list()
        self.__cur_zip_num: int = 0
        # self.__cur_zipfile: ZipFile = None  # TODO: None isn't allowed
        # self.__cur_zipfile: ZipFile  # TODO: None isn't allowed
        self.__cur_zip_size: int = 22  # 22 = size of empty zip file
        self.__failed_download_urls: Set[str] = set()
        self.mjson: MetaJSON = MetaJSON(self)
        # self.max_zip_size: int = 5875000000  # (47Gb)
        # self.max_zip_size: int = 2147483648  # 2GB
        # self.max_zip_size: int = 2000000000  # 2gig
        # self.max_zip_size: int = 8482560409  # 7.9GB
        self.max_zip_size: int = 8533180416  # 8.533GB (dvd9-knownGood)
        # self.max_zip_size: int = 8547621977  # 8.548GB (dvd9-untested)

    @property
    def num_threads(self) -> int:
        return self.__num_dl_threads

    @num_threads.setter
    def num_threads(self, num_threads: int):
        if num_threads > 0:
            self.__num_dl_threads = num_threads
        else:
            self.__num_dl_threads = 1

    def add_skip_urls(self, skip_urls) -> None:
        self.__skip_url_set.update(set(skip_urls))

    def remove_skip_urls(self, skip_urls) -> bool:
        try:
            self.__skip_url_set.remove(skip_urls)
            return True
        except KeyError:
            return False

    def get_skip_url_list(self) -> List:
        return list(self.__skip_url_set)

    def add_skip_regex(self, skip_regex) -> None:
        self.__skip_regex_set.update(set(skip_regex))

    def remove_skip_regex(self, skip_regex) -> bool:
        try:
            self.__skip_regex_set.remove(skip_regex)
            return True
        except KeyError:
            return False

    def get_skip_regex_list(self) -> List[str]:
        return list(self.__skip_regex_set)

    def add_skip_meta_zip(self, metazippath: Path) -> None:
        # open zip
        with ZipFile(metazippath) as metazip:
            # read in meta RepoFile(s)
            for metafilename in metazip.namelist():
                # print("parsing:")
                # print(metafilename)
                # print()
                metajsn = metazip.open(metafilename)
                # print(metajsn)
                # print(type(metajsn))
                jsndict = json.loads(metajsn.read())
                self.add_skip_meta_dict(jsndict)

    def add_skip_meta_dict(self, jsndict: Dict[str, Dict[str, str]]) -> None:
        # print("add_skip_meta_dict keys:", jsndict.keys())
        self.__skip_meta_dict.update(jsndict)

    def add_skip_meta_file(self, metafile: MetaData) -> None:
        # get the urls and highest sha
        for file in metafile.get_file_list():
            # call add_skip_meta for each
            hashes = file.get_hashes()
            shanum = 0
            for hashtype in hashes.keys():
                if hashtype.startswith("sha"):
                    if int(hashtype[3:]) > shanum:
                        shanum = int(hashtype[3:])
            self.add_skip_meta(
                metafile.get_remote_path(),
                "sha" + str(shanum),
                hashes["sha" + str(shanum)],
            )

    def add_skip_meta(
        self, url: str, checksum_type: str, checksum_val: str
    ) -> None:
        # print("add_skip_meta:", url)
        if url in self.__skip_meta_dict:
            self.__skip_meta_dict[url][checksum_type] = checksum_val
        else:
            self.__skip_meta_dict[url] = {checksum_type: checksum_val}

    def remove_skip_meta(self, url: str) -> None:
        self.__skip_meta_dict.pop(url, None)

    def get_skip_meta_dict(self) -> Dict[str, Dict[str, str]]:
        return self.__skip_meta_dict.copy()

    def add_download_files(self, file_list):
        for file in file_list:
            self.add_download_file(file)

    def add_download_file(self, file: RepoFile) -> None:
        print("add_download_file(", file.get_remote_path(), ")")
        # if file.opts["name"] == "Contents-arm64.gz":
        # if "libxshmfence" in file.get_remote_path():
        # dbg_exit(file.src)

        # check if file already in download list or is skipped
        if (
            file.get_remote_path() in self.__download_url_set
            or file.get_remote_path() in self.__skip_url_set
        ):
            # TODO: If file matches self.__skip_regex_set ....
            print("skipping:", file.get_remote_path())
            return
        # if file is part of a previous download set
        if self._file_in_skip_meta_dict(file):
            print(
                "Previously download '"
                + file.get_remote_path()
                + "', just adding to hash list"
            )
            self.mjson.addRepoFile(file)
        else:
            print("adding {} to download queue".format(file.get_remote_path()))
            self.__download_url_set.add(file.get_remote_path())
            self.__queued.put(file)

    def _file_in_skip_meta_dict(self, file: RepoFile) -> bool:
        # print("***_file_in_skip_meta_dict() for ", file.get_zip_path())
        checksum_name: str
        value: str
        for checksum_name, value in file.get_hashes().items():
            if (
                self.__skip_meta_dict
                and value
                and file.get_zip_path() in self.__skip_meta_dict.keys()
                and self.__skip_meta_dict[file.get_zip_path()]
                and self.__skip_meta_dict[file.get_zip_path()].get(
                    checksum_name
                )
                == value
            ):
                return True
        return False

    def _get_date_time(self) -> str:
        return self.date_time

    @staticmethod
    def __get_zipfile_for_pathname(path_name: str) -> ZipFile:
        if sys.version_info < (3, 7, 0):
            return ZipFile(
                path_name, mode="w", compression=ZIP_DEFLATED, allowZip64=True
            )
        else:  # python 3.7 added compresslevel
            return ZipFile(
                path_name,
                mode="w",
                compression=ZIP_DEFLATED,
                allowZip64=True,
                compresslevel=9,
            )

    def get_zipfile_for(
        self, number: int, name: str = "AptRepoDiff"
    ) -> ZipFile:
        zip_file_name = (
            "{0:0=4d}".format(number)
            + "_"
            + self._get_date_time()
            + "_"
            + name
            + ".zip"
        )
        zip_path_name = join(self.__dest_zip_dir, zip_file_name)
        return self.__get_zipfile_for_pathname(zip_path_name)

    def make_zips(self) -> None:
        # TODO: add check for size limit for meta zipfile.
        # while there are files to be saved into the zip(s)
        if "__cur_zipfile" not in locals().keys():
            self.__cur_zipfile: ZipFile = self.get_zipfile_for(
                self.__cur_zip_num
            )
        while len(self.__dl_threads) > 0 or self.__downloaded.qsize() > 0:
            print(
                "len(self.__dl_threads):",
                len(self.__dl_threads),
                "self.__downloaded.qsize():",
                self.__downloaded.qsize(),
            )
            if self.__downloaded.qsize() <= 0:
                sleep(5)
            else:
                try:
                    zfile: RepoFile = self.__downloaded.get(timeout=5)
                except Empty:
                    # Handle empty queue here
                    sleep(5)
                else:
                    if not zfile.raw_data:
                        if not zfile.is_metafile():
                            print(
                                "ERROR - File data missing for:",
                                zfile.get_remote_path(),
                            )
                            self.__failed_download_urls.add(
                                zfile.get_remote_path()
                            )
                        continue
                    # check if cur_zip_size ( current downloaded file size)
                    #   could cause the zip to go beyond the max
                    if (
                        self.__cur_zip_size + len(zfile.raw_data)
                        > self.max_zip_size
                        and len(self.__cur_zipfile.infolist()) > 0
                    ):

                        print("closing zip file #:", self.__cur_zip_num)
                        print(
                            "cur_zip_size:",
                            self.__cur_zip_size,
                            "len(zfile.raw_data):",
                            len(zfile.raw_data),
                        )
                        # TODO: for some reason it was giving a ValueError
                        #       closing the zip?  needs flushed?
                        # ERR: ValueError: Can't close the ZIP file while there
                        #       is an open writing handle on it.
                        #       Close the writing handle before closing the zip
                        try:
                            # self.__cur_zipfile.flush()
                            self.__cur_zipfile.close()
                        except ValueError:
                            self.__unclosed_zips.append(self.__cur_zipfile)

                        self.__cur_zip_num += 1
                        self.__cur_zipfile = self.get_zipfile_for(
                            self.__cur_zip_num
                        )
                        self.__cur_zip_size = 22  # size of empty zip file
                    # TODO: maybe keep all zip files open and keep list of open
                    #  space to see if the file will fit in an existing zip
                    #  file?
                    # self.__cur_zipfile.flush()
                    self.__cur_zipfile.writestr(
                        zfile.get_zip_path(), zfile.raw_data
                    )
                    print("zip_path", zfile.get_zip_path())
                    self.__cur_zip_size += self.__cur_zipfile.getinfo(
                        zfile.get_zip_path()
                    ).compress_size
                    # TODO: writing the json should also contain the skipped
                    #       files that were attempted to download.
                    self.mjson.addRepoFile(zfile)

        # self.__meta_zipfile.writestr("FailedDownloads.list",
        #                             '/n'.join(self.__failed_download_urls))
        if len(self.__failed_download_urls) > 0:
            print(
                "FailedDownloads.list", "/n".join(self.__failed_download_urls)
            )
        else:
            print("No failed downloads")
        print("__dest_zip_dir:", self.__dest_zip_dir)

        # TODO: make MetaJSON dump per file for future resume purposes
        self.mjson.dump_to_zip()

        # TODO if this closes fails (exception) does anything bad
        #  happen?  We are exiting anyway.
        self.__cur_zipfile.close()

    def __downloader(self) -> None:
        while True:
            try:
                file: RepoFile = self.__queued.get(timeout=5)
            except Empty:
                print("__downloader() empty queue, taking nap")
                sleep(30)
            else:
                print("__downloader() downloading:", file.get_remote_path())
                file.download()
                # TODO: Does a failed download get added? is it bad?
                self.__downloaded.put(file)
                if file.is_metafile():
                    print("downloaded/processing metafile:", file)
                    self.add_download_files(MetaData(file).get_file_list())
                self.__queued.task_done()
            if len(self.__dl_threads) > self.__num_dl_threads:
                break

    def __create_threads(self, count: int) -> None:
        for i in range(count):
            # TODO: fix possible duplicate names if called again
            print("creating dl thread: ", i)
            t = Thread(target=self.__downloader, name="Dl#" + str(i))
            self.__dl_threads.append(t)
            t.daemon = True
            t.start()

        print("created dl threads")
        self.__zip_thread = Thread(target=self.make_zips, name="ZipThread")
        self.__zip_thread.daemon = True
        self.__zip_thread.start()
        print("created zip thread")

    def start_downloading(self, dest_zip_dir: str) -> None:
        self.__dest_zip_dir = dest_zip_dir
        self.__create_threads(self.__num_dl_threads)
        sleep(60)
        print("just took first nap, queued size:", self.__queued.qsize())

        # TODO: if a file being downloaded is a meta file, more files may be
        # coming, But the queue maybe be empty and we go to close the threads.
        # Need queue empty and all d/l threads waiting?
        while not self.__queued.empty():
            for thread in list(self.__dl_threads):
                if not thread.isAlive():
                    self.__dl_threads.remove(thread)
                    thread.join()  # needed?
            if len(self.__dl_threads) < self.__num_dl_threads:
                self.__create_threads(
                    self.__num_dl_threads - len(self.__dl_threads)
                )
            sleep(60)
            print("thread mgmt while queued size:", self.__queued.qsize())

        print("closing time thread count:", len(self.__dl_threads))
        self.__num_dl_threads = 0
        sleep(5)
        for thread in self.__dl_threads:
            print("alive?", thread.isAlive())
            thread.join()
            # I'd like to remove them here, but changing a list you are
            # iterating over is bad right?
        # Pytype didn't like the clear() (even using version 3.6)
        # self.__dl_threads.clear()
        # self.__dl_threads *=0
        del self.__dl_threads[:]

        print("closing zip")
        self.__zip_thread.join()
        # TODO: is setting the thread to None needed?
        # self.__zip_thread = None
        for zf in self.__unclosed_zips:
            zf.close()
        print("finished downloading")


class MetaJSON:
    """
        Simple class to store the downloaded file list and checksums
    """

    __file_chksums: Dict[str, Dict[str, str]] = dict()

    def __init__(self, dlf: DownloadFiles):
        self.dlf = dlf

    def addRepoFile(self, zfile: RepoFile) -> None:
        self.addPathChecksums(
            zfile.get_zip_path(), Checksums.calculate_defaults(zfile.raw_data)
        )

    def addPathChecksums(
        self, pathname: str, checksums: Dict[str, str]
    ) -> None:
        if pathname in self.__file_chksums.keys():
            self.__file_chksums[pathname].update(checksums)
        else:
            self.__file_chksums[pathname] = checksums

    def dump_to_zip(self) -> None:
        with self.dlf.get_zipfile_for(0, "FileList") as filelist:
            filelist.writestr(
                "file_list.json", json.dumps(self.__file_chksums, indent=0)
            )


def dbg_exit(txt: str = "") -> None:
    import traceback
    import os
    import sys

    traceback.print_stack(file=sys.stdout)
    print("TRACEME", txt)
    sys.stdout.flush()
    sys.stderr.flush()
    os._exit(0)


if __name__ == "__main__":
    lp = ListParser()
    date_today = date.today().isoformat()
    """
    line = "deb [arch=mips,i386 arch-=mips]
            http://example.org/debian stable rocks"
    """
    """
    line = "deb file:///Users/master/Documents/atom/apt-mirror/" + \
        "us.archive.ubuntu.com/ubuntu/ bionic main"
    print(line)
    src = lp.parse_line(line)
    print("src: " + str(src))
    df = DownloadFiles()
    for mfile in src.get_all_base_metadata_repofiles():
        print("release_files: ", mfile)
        df.add_download_file(mfile)
        break
    df.start_downloading("/tmp")
    print("Done!")
    exit(0)
"""
    # sources_list_filename = "real.sources.list"
    # sources_list_filename = "small.sources.list"
    sources_list_filename = "small.sources.list"
    df = DownloadFiles()
    for src in lp.parse_file(Path(sources_list_filename)):
        print("src: " + str(src))
        for mfile in src.get_all_base_metadata_repofiles():
            print("MAIN -> release_files: ", mfile)
            df.add_download_file(mfile)
    df.add_skip_meta_zip(Path("./file_list.zip"))
    # df.add_skip_meta_zip("0000_2019-06-28_14:51_FileList.zip")
    df.start_downloading(".")
    print("Done!")
